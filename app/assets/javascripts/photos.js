
//GET /api/photos

// Some jQuery
$(document).ready(function(){

  $('.photo img').each(function(e){
    $(this).css('border','2px solid '+getRandomColor() + '');
  });

});


function getRandomColor() {
  var letters = '0123456789ABCDEF'.split('');
  var color = '#';
  for (var i = 0; i < 6; i++ ) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}